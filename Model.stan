data {
     int<lower=1> N; //Antalet datapoints
     vector[N] y; //independet
     vector[N] x; //dependent
}
    
parameters {
  vector[2] beta;
  real<lower=0> sigma;
}

model {
y ~ normal(beta[1]+ beta[2] * x + beta[2] * x^2, sigma);
}

generated quantities {
  vector[N] new_y;
  new_y <- beta[1] + beta[2]*x
}

data {
     int<lower=1> N; //Antalet datapoints
     vector[N] y; //independet
     vector[N] x; //dependent
}
    
parameters {
  vector[2] beta;
  real<lower=0> sigma;
}

model {
beta[1] ~ normal(0,1000) ;
  
y ~ normal(beta[1]+ beta[2] * x, sigma);
}




data {
     int<lower=1> N; //Antalet datapoints
     vector[N] y; //independet
     real[N] x; //dependent
}
    
parameters {
  real alpha;
  real beta;
  real beta2;
  real<lower=0> sigma;
}

model {
  y ~ normal(alpha + beta * x + beta2 * (x) ^ 2, sigma);
}


